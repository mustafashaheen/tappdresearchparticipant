//
//  PasscodeViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 14/05/2018.
//  Copyright © 2018 UNMC. All rights reserved.
//

import UIKit

class PasscodeViewController: UIViewController {

    var alphaCode: String = "PD!zaw3sOme"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore  {
            print("Not first launch.")
            showViewController()
        } else {
            print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            askAlphaNumericCode()
        }
    }
    func askAlphaNumericCode(){
        
        var newAplhaCode = ""
        let alert = UIAlertController(title: "Enter Alpha Numeric Code",
                                      message: "Please enter alpha numeric code provided by the owner of the application",
                                      preferredStyle: .alert)
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .default
            textField.keyboardType = .default
            textField.autocorrectionType = .default
            textField.placeholder = "Enter code here"
            textField.clearButtonMode = .whileEditing
            textField.isSecureTextEntry = true
        }
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
            // Get 1st TextField's text
            let textField = alert.textFields![0]
            newAplhaCode = textField.text!
            
            self.compareAlphaCode(enteredCode: newAplhaCode)
        })
        alert.addAction(submitAction)
        present(alert, animated: true, completion: nil)
    }
    func compareAlphaCode(enteredCode: String){
        if(enteredCode != alphaCode){
            let alert = UIAlertController(title: "Failure!!", message: "Alpha Numeric Code entered does not match", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: askAlphaAgain))
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Success!", message: "Alpha Numeric Code Accepted", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: showViewController))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func askAlphaAgain(action: UIAlertAction)
    {
        var newAplhaCode = ""
        let alert = UIAlertController(title: "Enter Alpha Numeric Code",
                                      message: "Please enter alpha numeric code provided by the owner of the application",
                                      preferredStyle: .alert)
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .default
            textField.keyboardType = .default
            textField.autocorrectionType = .default
            textField.placeholder = "Enter code here"
            textField.clearButtonMode = .whileEditing
            textField.isSecureTextEntry = true
        }
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
            // Get 1st TextField's text
            let textField = alert.textFields![0]
            newAplhaCode = textField.text!
            
            self.compareAlphaCode(enteredCode: newAplhaCode)
        })
        alert.addAction(submitAction)
        present(alert, animated: true, completion: nil)
        
    }
    func showViewController(action: UIAlertAction){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigationViewController") as! MainNavigationViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
    func showViewController(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigationViewController") as! MainNavigationViewController
        self.present(nextViewController, animated:true, completion:nil)
    }

}
