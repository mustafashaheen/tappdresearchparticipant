//
//  BoxWebViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 14/03/2018.
//  Copyright © 2018 UNMC. All rights reserved.
//

import UIKit
import WebKit
class BoxWebViewController: UIViewController {

    @IBOutlet weak var boxWebView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://account.box.com/api/oauth2/authorize?response_type=code&client_id=elyaz5phkslvcxlfuhn0r2kkyj5dm293&redirect_uri=https://box.mustafashaheen.me/token&state=security_token%3DKnhMJatFipTAnM0nHlZA")
        let request = URLRequest(url: url!)
        
        boxWebView.load(request)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
