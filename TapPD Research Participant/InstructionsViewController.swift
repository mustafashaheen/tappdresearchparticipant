//
//  InstructionsViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 22/12/2017.
//  Copyright © 2017 UNMC. All rights reserved.
//

import UIKit

class InstructionsViewController: UIViewController {

    @IBOutlet var startTaskButton: RoundedButton!
    var choice = 0
    var task = 0
    var visit = 0
    var created = false
    override func viewDidLoad() {
        super.viewDidLoad()
        print("choice in instructions is: \(choice)")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        if(created == true)
        {
           performSegue(withIdentifier: "backToSurveyAfterInstructions", sender: self)
        }else{
            performSegue(withIdentifier: "backToViewControllerAfterInstructions", sender: self)
        }
        
    }
    
    @IBAction func startTaskButtonPressed(_ sender: RoundedButton) {
        performSegue(withIdentifier: "specialInstructionsAfterInstructions", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "specialInstructionsAfterInstructions")
        {
        var mainController = segue.destination as! SpecialInstructionsViewController
        mainController.choice = choice
        mainController.visit = visit
        mainController.task = task
        }else if(segue.identifier == "backToSurveyAfterInstructions") {
            var mainController = segue.destination as! SurveyViewController
            mainController.choice = choice
            mainController.visit = visit
            mainController.task = task
            mainController.created = created
        }else{
            var mainController = segue.destination as! ViewController
        }
        
    }
}
