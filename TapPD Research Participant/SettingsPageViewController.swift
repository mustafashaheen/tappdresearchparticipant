//
//  SettingsPageViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 14/12/2017.
//  Copyright © 2017 UNMC. All rights reserved.
//

import UIKit
import UserNotifications
import Just

class SettingsPageViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    var temp: Int = 0
    @IBOutlet var dfrh: UIButton!
    @IBOutlet var dflh: UIButton!
    @IBOutlet var sfrh: UIButton!
    @IBOutlet var sflh: UIButton!
    @IBOutlet var saveButton: RoundedButton!
    @IBOutlet var stopAlarmTextField: UITextField!
    @IBOutlet var startAlarmRangeTextField: UITextField!
    @IBOutlet var taskRangeTextField: UITextField!
    @IBOutlet var subjectIdTextField: UITextField!
    @IBOutlet var researchIdTextField: UITextField!
    @IBOutlet var timeBetweenAlarmsTextField: UITextField!
    let datePicker = UIDatePicker()
    let picker = UIPickerView()
    let taskRangePicker = UIPickerView()
    var count = 0
    let activities = ["MBHL"]
    var range = [String](repeating: "", count: 50)
    var tryCountDefault = UserDefaults.standard
    var dfrhDefault = UserDefaults.standard
    var dflhDefault = UserDefaults.standard
    var sfrhDefault = UserDefaults.standard
    var sflhDefault = UserDefaults.standard
    var stopAlarmDefault = UserDefaults.standard
    var startAlarmDefault = UserDefaults.standard
    var taskRangeDefault = UserDefaults.standard
    var subjectIdDefault = UserDefaults.standard
    var researchIdDefault = UserDefaults.standard
    var timeBtwAlarmsDefault = UserDefaults.standard
    var folderIdDefault = UserDefaults.standard
    var tryCount = 0
    var indexDefault = UserDefaults.standard
    var startTime = Date()
    var stopTime = Date()
    var accessToken = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let result = Just.get("http://mustafashaheen6951.cloudapp.net:3000/start")
        let json = result.json as! NSDictionary
        let token = json["token"] as! NSString
        accessToken = token as String
        picker.tag = 1
        taskRangePicker.tag = 2
        var i = 1
        while (i <= 50)
        {
            range[i - 1] = String(i)
            i += 1
        }
        if(dfrhDefault.value(forKey: "dfrh") != nil){
            var dfrhTemp = dfrhDefault.value(forKey: "dfrh") as! Bool
            if(dfrhTemp == true){
                dfrh.setImage(UIImage(named: "checkedbox"), for: .normal)
            }
        }
        if(dflhDefault.value(forKey: "dflh") != nil){
            var dflhTemp = dflhDefault.value(forKey: "dflh") as! Bool
            if(dflhTemp == true){
                dflh.setImage(UIImage(named: "checkedbox"), for: .normal)
            }
        }
        if(sflhDefault.value(forKey: "sflh") != nil){
            var sflhTemp = sflhDefault.value(forKey: "sflh") as! Bool
            if(sflhTemp == true){
                sflh.setImage(UIImage(named: "checkedbox"), for: .normal)
            }
        }
        if(sfrhDefault.value(forKey: "sfrh") != nil){
            var sfrhTemp = sfrhDefault.value(forKey: "sfrh") as! Bool
            if(sfrhTemp == true){
                sfrh.setImage(UIImage(named: "checkedbox"), for: .normal)
            }
        }
        if(stopAlarmDefault.value(forKey: "stopalarm") != nil){
            stopAlarmTextField.text = stopAlarmDefault.value(forKey: "stopalarm") as! String

        }
        if(startAlarmDefault.value(forKey: "startalarm") != nil){
            startAlarmRangeTextField.text = startAlarmDefault.value(forKey: "startalarm") as! String
            
        }
        if(taskRangeDefault.value(forKey: "taskrange") != nil){
            
            taskRangeTextField.text = taskRangeDefault.value(forKey: "taskrange") as! String
            
        }
        if(subjectIdDefault.value(forKey: "subjectid") != nil){
            subjectIdTextField.text = subjectIdDefault.value(forKey: "subjectid") as! String
            
        }
        if(researchIdDefault.value(forKey: "researchid") != nil){
            researchIdTextField.text = researchIdDefault.value(forKey: "researchid") as! String
            
        }
        if(timeBtwAlarmsDefault.value(forKey: "timebetween") != nil){
            timeBetweenAlarmsTextField.text = timeBtwAlarmsDefault.value(forKey: "timebetween") as! String
            
        }
        picker.delegate = self
        taskRangePicker.delegate = self
        createTaskPicker()
        createDatePicker()
        createPicker()
        createDoneButton()
        createSubjectDoneButton()
        // Do any additional setup after loading the view.
    }
    func createTaskPicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,target: nil,action: #selector(pickerDonePressed))
        toolbar.setItems([doneButton],animated: false)
        taskRangeTextField.inputAccessoryView = toolbar
    }
    func createSubjectDoneButton()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,target: nil,action: #selector(subjectIdDonePressed))
        toolbar.setItems([doneButton],animated: false)
        subjectIdTextField.inputAccessoryView = toolbar
    }
    @objc func subjectIdDonePressed()
    {
        self.view.endEditing(true)
    }
    @objc func pickerDonePressed(){
        
        self.view.endEditing(true)
    }
    @objc func timeBetweenDonePressed()
    {
        self.view.endEditing(true)
    }
    func createPicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,target: nil,action: #selector(pickerDonePressed))
        toolbar.setItems([doneButton],animated: false)
        researchIdTextField.inputAccessoryView = toolbar
        
    }
    func createDoneButton()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,target: nil,action: #selector(timeBetweenDonePressed))
        toolbar.setItems([doneButton],animated: false)
        timeBetweenAlarmsTextField.inputAccessoryView = toolbar
    }
    func createDatePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,target: nil,action: #selector(donePressed))
        toolbar.setItems([doneButton],animated: false)
        startAlarmRangeTextField.inputAccessoryView = toolbar
        stopAlarmTextField.inputAccessoryView = toolbar
        datePicker.datePickerMode = UIDatePickerMode.time
        datePicker.minuteInterval = 30
    }
    @IBAction func startEditBegin(_ sender: UITextField) {
        startAlarmRangeTextField.inputView = datePicker
        count = 1
    }
    @objc func donePressed(){
        let date = datePicker.date
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let result = formatter.string(from: date)
        if(count == 1){
            startAlarmRangeTextField.text = result
            startTime = date
        }
        else
        {
            stopAlarmTextField.text = result
            stopTime = date
            
        }
        self.view.endEditing(true)
    }
    
    @IBAction func saveButtonPressed(_ sender: RoundedButton) {

        var min = 30
        var max = 12 * 60
        if(!((timeBetweenAlarmsTextField.text?.isEmpty)!))
        {
            temp = Int(timeBetweenAlarmsTextField.text!)!
        }
        if(researchIdTextField.text?.isEmpty)!{
            displayAlram(message: "Research ID field cannot be empty")
        }else if(subjectIdTextField.text?.isEmpty)!{
            displayAlram(message: "Subject ID field cannot be empty")
        }
        else if(startAlarmRangeTextField.text?.isEmpty)!{
            displayAlram(message: "Start Alarm Range field cannot be empty")
        }else if(stopAlarmTextField.text?.isEmpty)!{
            displayAlram(message: "Stop Alarm Range field cannot be empty")
        }
        else if(timeBetweenAlarmsTextField.text?.isEmpty)!{
            displayAlram(message: "Time Between Alarms field cannot be empty")
        } else if((dfrh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!) && ((sfrh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!) && ((dflh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!) && ((sflh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!){
            displayAlram(message: "You must choose at least one task")
        } else if(stopTime < startTime)
        {
            displayAlram(message: "Alarm stop time cannot be less than alarm start time")
        }else
            if((temp < min) || (temp > max))
        {
            displayAlram(message: "The alarm range can be between 30 mins to 12 hours")
        }else{
            if(sflh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!
            {
                sflhDefault.set(false, forKey: "sflh")
            }else{
                sflhDefault.set(true, forKey: "sflh")
                tryCount += 1
            }
            if(sfrh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!
            {
                sfrhDefault.set(false, forKey: "sfrh")
            }else{
                sflhDefault.set(true, forKey: "sfrh")
                tryCount += 1
            }
            if(dflh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!
            {
                dflhDefault.set(false, forKey: "dflh")
            }else{
                dflhDefault.set(true, forKey: "dflh")
                tryCount += 1
            }
            if(dfrh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!
            {
                dfrhDefault.set(false, forKey: "dfrh")
            }else{
                dfrhDefault.set(true, forKey: "dfrh")
                tryCount += 1
            }
            
                taskRangeDefault.set(taskRangeTextField.text, forKey: "taskrange")
            tryCountDefault.set(tryCount, forKey: "trycount")
            timeBtwAlarmsDefault.set(timeBetweenAlarmsTextField.text, forKey: "timebetween")
            researchIdDefault.set(researchIdTextField.text, forKey: "researchid")
            startAlarmDefault.set(startAlarmRangeTextField.text, forKey: "startalarm")
            stopAlarmDefault.set(stopAlarmTextField.text, forKey: "stopalarm")
           subjectIdDefault.set(subjectIdTextField.text, forKey: "subjectid")
                createNotifications()
                createFolder()
            showSuccessAlert()
            
        }
        
    }
    
    func createNotifications()
    {
        var notificationStopAlarm = ""
        var notificationStartAlarm = ""
        var notificationInterval: Int = 0
        if(timeBtwAlarmsDefault.value(forKey: "timebetween") != nil){
            notificationInterval = Int(timeBtwAlarmsDefault.value(forKey: "timebetween") as! String)!
        }
        if(stopAlarmDefault.value(forKey: "stopalarm") != nil){
            notificationStopAlarm = stopAlarmDefault.value(forKey: "stopalarm") as! String
        }
        if(startAlarmDefault.value(forKey: "startalarm") != nil){
            notificationStartAlarm = startAlarmDefault.value(forKey: "startalarm") as! String
        }
        
        notificationInterval = 2 //remove later

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        var dateStart = dateFormatter.date(from: notificationStartAlarm)
        print("Date Start: \(String(describing: dateStart))")
        let dateEnd = dateFormatter.date(from: notificationStopAlarm)
        print("Date Start: \(String(describing: dateEnd))")
        
        while(dateStart! < dateEnd!){
            let localNotification = UILocalNotification()
            localNotification.fireDate = dateStart
            localNotification.alertTitle = "Reminder"
            localNotification.alertBody = "Please complete your daily tasks"
            localNotification.applicationIconBadgeNumber = localNotification.applicationIconBadgeNumber + 1
            localNotification.timeZone = TimeZone.current
            localNotification.repeatInterval = NSCalendar.Unit.day
            localNotification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(localNotification)
            
            dateStart = dateStart?.addingTimeInterval(TimeInterval(notificationInterval * 60))
        }
        
    }
    func showSuccessAlert(){
        let alert = UIAlertController(title: "SUCCESS!", message: "Settings have been saved for the research participant", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: openController))
        self.present(alert, animated: true, completion: nil)
    }
    func openController(action: UIAlertAction){
        performSegue(withIdentifier: "viewControllerAfterSettings", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var mainController = segue.destination as! ViewController
        mainController.tryCount = tryCount
        mainController.settings = true
        
    }
    
    
    func newNotificaitonWork()
    {
        var date = Date()
        print(date)
        
        let localNotification = UILocalNotification()
        localNotification.fireDate = date
        localNotification.alertTitle = "Reminder"
        localNotification.alertBody = "Please complete your daily tasks"
        localNotification.applicationIconBadgeNumber = localNotification.applicationIconBadgeNumber + 1
        localNotification.timeZone = TimeZone.current
        localNotification.repeatInterval = NSCalendar.Unit.day
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    
    func displayAlram(message: String){
        let alert = UIAlertController(title: "Failure!", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func stopBeginEdit(_ sender: UITextField) {
        
        stopAlarmTextField.inputView = datePicker
        count = 2
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if(pickerView.tag == 1)
        {
        return activities.count
        }else{
            return range.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 1)
        {
            return activities[row]
        }
        else{
            return range[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 1)
        {
            researchIdTextField.text = activities[row]
        }else{
            taskRangeTextField.text = range[row]
        }
        
    }
    
    @IBAction func checkBoxButtonPressed(_ sender: UIButton) {
        
        if(sender.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!{
            sender.setImage(UIImage(named: "checkedbox"), for: .normal)
        }
        else{
            sender.setImage(UIImage(named: "uncheckedbox"), for: .normal)
        }
    }
    @IBAction func taskRangeBeginEditing(_ sender: UITextField) {
        taskRangeTextField.inputView = taskRangePicker
    }
    @IBAction func researchEditingBegin(_ sender: UITextField) {
        researchIdTextField.inputView = picker
    }
    @IBAction func sflhPressed(_ sender: UIButton) {
        
        if(sflh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!{
            sflh.setImage(UIImage(named: "checkedbox"), for: .normal)
        }
        else{
            sflh.setImage(UIImage(named: "uncheckedbox"), for: .normal)
        }
    }
    
    @IBAction func sfrhPressed(_ sender: UIButton) {
        if(sfrh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!{
            sfrh.setImage(UIImage(named: "checkedbox"), for: .normal)
        }
        else{
            sfrh.setImage(UIImage(named: "uncheckedbox"), for: .normal)
        }
    }
    
    @IBAction func dflhPressed(_ sender: UIButton) {
        if(dflh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!{
            dflh.setImage(UIImage(named: "checkedbox"), for: .normal)
        }
        else{
            dflh.setImage(UIImage(named: "uncheckedbox"), for: .normal)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MainNavigationViewController") as! MainNavigationViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
    @IBAction func dfrhPressed(_ sender: UIButton) {
        if(dfrh.currentImage?.isEqual(UIImage(named: "uncheckedbox")))!{
            dfrh.setImage(UIImage(named: "checkedbox"), for: .normal)
        }
        else{
            dfrh.setImage(UIImage(named: "uncheckedbox"), for: .normal)
        }
    }
    func createFolder()
    {
        var subjectId = subjectIdTextField.text!
        var folderId = ""
        folderId = searchFolder(subjectId: subjectId)
        if(folderId == "")
        {
        let endpoint = "https://api.box.com/2.0/folders"

            let finalDict = ["name": "\(subjectId)", "parent": ["id": 10971544178]] as [String : Any]
            let createFolderResult = Just.post(endpoint, json:finalDict, headers:["Authorization":"Bearer \(accessToken)", "content-type":"application/json"])
        
            let json = createFolderResult.json as! NSDictionary
        
            folderId = json["id"] as! String
        }
            folderIdDefault.set(folderId, forKey: "folderid")


    }
    func searchFolder(subjectId: String) -> String
    {
        var id = ""
        let folderSearch = Just.get("https://api.box.com/2.0/search?query=\(subjectId)&type=folder", headers:["Authorization":"Bearer \(accessToken)"])
        let json = folderSearch.json as! NSDictionary
        let entries = json["entries"] as! Array<Dictionary<String, Any>>
        if(entries.count > 0)
        {
            id = entries[0]["id"] as! String
        }
        return id
    }
    
    func getRightComponent(component: Int) -> Int
    {
        if(component < 0)
        {
            return 24 - component
        }
        else{
            return component
        }
    }
}
