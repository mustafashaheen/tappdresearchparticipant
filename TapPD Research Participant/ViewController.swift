//
//  ViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 11/12/2017.
//  Copyright © 2017 UNMC. All rights reserved.
//

import UIKit
import CoreData
import Just
import UserNotifications
class ViewController: UIViewController {

    let passcode = "12345678"
    var count = 0
    var send = false
    var tryCount = 0
    var tempTryCount = 0
    var tempSflh = false
    var tempSfrh = false
    var tempDflh = false
    var tempDfrh = false
    var choice: Int = 0
    var tryCountDefault = UserDefaults.standard
    var dfrhDefault = UserDefaults.standard
    var dflhDefault = UserDefaults.standard
    var sfrhDefault = UserDefaults.standard
    var sflhDefault = UserDefaults.standard
    var stopAlarmDefault = UserDefaults.standard
    var startAlarmDefault = UserDefaults.standard
    var taskRangeDefault = UserDefaults.standard
    var subjectIdDefault = UserDefaults.standard
    var researchIdDefault = UserDefaults.standard
    var timeBtwAlarmsDefault = UserDefaults.standard
    var created = false
    var taskRange: Int = 0
    var tempTaskRange = ""
    var subjectId = ""
    var accessToken = ""
    var folderId = ""
    var folderIdDefault = UserDefaults.standard
    var settings = false
    override func viewDidAppear(_ animated: Bool) {
        if(send == true)
        {
            if(subjectIdDefault.value(forKey: "subjectid") != nil){
                subjectId = subjectIdDefault.value(forKey: "subjectid") as! String
            }
            if(folderIdDefault.value(forKey: "folderid") != nil)
            {
                folderId = folderIdDefault.value(forKey: "folderid") as! String
            }
            let result = Just.get("http://mustafashaheen6951.cloudapp.net:3000/start")
            let json = result.json as! NSDictionary
            let token = json["token"] as! NSString
            accessToken = token as String
            var topViewController = UIApplication.shared.delegate!.window!!.rootViewController!
            while (topViewController.presentedViewController != nil){
                topViewController = topViewController.presentedViewController!
            }
            
            // Usage of GCD here is only necessary because it's called from
            // viewDidLoad. If called by viewDidAppear or some action callback, you
            // don't need it:
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Saving Progress", message: "Please Wait", preferredStyle: .alert)
                let progressBar = UIProgressView(progressViewStyle: .default)
                progressBar.setProgress(0.0, animated: true)
                progressBar.frame = CGRect(x: 10, y: 70, width: 250, height: 0)
                alert.view.addSubview(progressBar)
                topViewController.present(alert, animated: true, completion: nil)
                var progress: Float = 0.0
                
                // Do the time critical stuff asynchronously
                
                DispatchQueue.global(qos: .background).async {
//                    self.uploadFile()
                    self.uploadFile(completion: { (status) in
                        DispatchQueue.main.async {
                            
                            alert.dismiss(animated: true, completion: nil);
                        }
                    })
                    repeat {
                        progress += 0.2
                        Thread.sleep(forTimeInterval: 0.25)
                        DispatchQueue.main.async(flags: .barrier) {
                            progressBar.setProgress(progress, animated: true)
                        }
                    } while progress < 1.0
                }
            }
            
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        
        if(tryCountDefault.value(forKey: "trycount") != nil){
            tryCount = tryCountDefault.value(forKey: "trycount") as! Int
            
        }
        if(taskRangeDefault.value(forKey: "taskrange") != nil){
            tempTaskRange = taskRangeDefault.value(forKey: "taskrange") as! String
            taskRange = Int(tempTaskRange)!
        }
       /* var temp: Int = 0
        var stopAlarm = ""
        var startAlarm = ""
        if(settings == true)
        {
            if(timeBtwAlarmsDefault.value(forKey: "timebetween") != nil){
                temp = Int(timeBtwAlarmsDefault.value(forKey: "timebetween") as! String)!
            }
            if(stopAlarmDefault.value(forKey: "stopalarm") != nil){
                stopAlarm = stopAlarmDefault.value(forKey: "stopalarm") as! String
            }
            if(startAlarmDefault.value(forKey: "startalarm") != nil){
                startAlarm = startAlarmDefault.value(forKey: "startalarm") as! String
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            var fullDate = dateFormatter.date(from: stopAlarm)
            var dateStamp = fullDate!.timeIntervalSince1970
            var stopDate = Int(dateStamp)
            fullDate = dateFormatter.date(from: startAlarm)
            dateStamp = fullDate!.timeIntervalSince1970
            var startDate = Int(dateStamp)
            var currentDate = Date()
            let newDate = dateFormatter.string(from: currentDate)
            currentDate = dateFormatter.date(from: newDate)!
            let interval = Int(currentDate.timeIntervalSince1970)
            if((interval > startDate) && (interval < stopDate))
            {
               //saveNotification()
                
                let content = UNMutableNotificationContent()
                content.title = "Reminder!!"
                content.body = "Please complete your daily tasks"
                content.badge = 1
                temp = temp * 60
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(temp),repeats: true)
                let request = UNNotificationRequest(identifier: "timerDone",content: content,trigger: trigger)
                UNUserNotificationCenter.current().add(request,withCompletionHandler: nil)
                
            }
        }*/
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var request = NSFetchRequest<NSFetchRequestResult>(entityName: "Visit")
        request.returnsObjectsAsFaults = false
        do{
            let results = try context.fetch(request)
            count = results.count
        }
        catch
        {
            
        }
        request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        request.returnsObjectsAsFaults = false
        do{
            let results = try context.fetch(request)
            if results.count > 0
            {
                
                for result in results as! [NSManagedObject]
                {
                    if let tryNumber = result.value(forKey: "number") as? Int
                    {
                        if let visitNumber = result.value(forKey: "visitnumber") as? Int
                        {
                            if(visitNumber == count){
                                tempTryCount += 1
                                if let finger = result.value(forKey: "finger") as? String
                                {
                                    if let hand = result.value(forKey: "hand") as? String
                                    {
                                        if(finger == "single") && (hand == "left")
                                        {
                                            tempSflh = true
                                        }
                                        if(finger == "single") && (hand == "right")
                                        {
                                            tempSfrh = true
                                        }
                                        if(finger == "double") && (hand == "left")
                                        {
                                            tempDflh = true
                                        }
                                        if(finger == "double") && (hand == "right")
                                        {
                                            tempDfrh = true
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        catch
        {
            
        }
        if((tryCount == tempTryCount) || (count == 0)) 
        {
            tempTryCount = 0
            tempSflh = false
            tempSfrh = false
            tempDflh = false
            tempDfrh = false
            count += 1
            
            created = true
            
        }
    }
    func saveNotification()
    {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        var request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notification")
        request.returnsObjectsAsFaults = false
        do{
            let results = try context.fetch(request)
            var count = results.count
            count += 1
            let currentDate = Date()
            let interval = Int(currentDate.timeIntervalSince1970)
            let newNotification = NSEntityDescription.insertNewObject(forEntityName: "Notification", into: context)
            newNotification.setValue(count, forKey: "number")
            newNotification.setValue(interval, forKey: "date")
            
            do{
                try context.save()
                
            }catch{
                
            }
        }
        catch
        {
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        showPasswordAlert()
    }
    func showPasswordAlert() {
        let alert = UIAlertController(title: "Enter Passcode",
                                      message: "Please enter your passcode",
                                      preferredStyle: .alert)
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .default
            textField.keyboardType = .numberPad
            textField.autocorrectionType = .default
            textField.placeholder = "Enter your passcode here:"
            textField.clearButtonMode = .whileEditing
            textField.isSecureTextEntry = true
        }
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
            // Get 1st TextField's text
            let textField = alert.textFields![0]
            self.comparePasscode(pass: textField.text!)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(submitAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    func comparePasscode(pass: String)
    {
        if(pass == self.passcode){
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SettingsNavigationViewController") as! SettingsNavigationViewController
            self.present(nextViewController, animated:true, completion:nil)
        }
        else{
            showError()
        }
    }
    func showPasswordAlert(action: UIAlertAction) {
        let alert = UIAlertController(title: "Enter Passcode",
                                      message: "Please enter your passcode",
                                      preferredStyle: .alert)
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .default
            textField.keyboardType = .numberPad
            textField.autocorrectionType = .default
            textField.placeholder = "Enter your passcode here:"
            textField.clearButtonMode = .whileEditing
            textField.isSecureTextEntry = true
        }
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
            // Get 1st TextField's text
            let textField = alert.textFields![0]
            self.comparePasscode(pass: textField.text!)
            
        })
        alert.addAction(submitAction)
        present(alert, animated: true, completion: nil)
    }
    func showError(){
        let alert = UIAlertController(title: "ERROR!!", message: "Wrong Passcode", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: showPasswordAlert))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func startTaskButtonPressed(_ sender: RoundedButton) {
        
        
        if(taskRange == 0)
        {
            let alert = UIAlertController(title: "Settings Not Set", message: "Please set the details in the settings page first", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
        var sflhDefault = UserDefaults.standard
        var sfrhDefault = UserDefaults.standard
        var dflhDefault = UserDefaults.standard
        var dfrhDefault = UserDefaults.standard
        
        var sflh: Bool = false
        var sfrh: Bool = false
        var dflh: Bool = false
        var dfrh: Bool = false
        if(sflhDefault.value(forKey: "sflh") != nil){
            sflh = sflhDefault.value(forKey: "sflh") as! Bool
        }
        if(sfrhDefault.value(forKey: "sfrh") != nil){
            sfrh = sfrhDefault.value(forKey: "sfrh") as! Bool
        }
        if(dflhDefault.value(forKey: "dflh") != nil){
            dflh = dflhDefault.value(forKey: "dflh") as! Bool
        }
        if(dfrhDefault.value(forKey: "dfrh") != nil){
            dfrh = dfrhDefault.value(forKey: "dfrh") as! Bool
        }
        if(count == taskRange)
        {
                var loop = false
                while(loop == false)
                {
                var ran = arc4random_uniform(4) + 1
                switch(ran){
                case 1: if(sflh == true){
                    if(tempSflh == false)
                    {
                        self.choice = 1
                        loop = true
                        if(created == true)
                        {
                        performSegue(withIdentifier: "surveyAfterViewController", sender: self)
                        }else {
                            performSegue(withIdentifier: "instructionsAfterViewController", sender: self)
                        }
                        
                    }
                    }
                    break
                case 2: if(sfrh == true)
                {
                    if(tempSfrh == false)
                    {
                        self.choice = 2
                        loop = true
                        if(created == true)
                        {
                            performSegue(withIdentifier: "surveyAfterViewController", sender: self)
                        }else {
                            performSegue(withIdentifier: "instructionsAfterViewController", sender: self)
                        }
                        
                    }
                }
                    break
                case 3: if(dflh == true)
                {
                    if(tempDflh == false)
                    {
                        self.choice = 3
                        loop = true
                        if(created == true)
                        {
                            performSegue(withIdentifier: "surveyAfterViewController", sender: self)
                        }else {
                            performSegue(withIdentifier: "instructionsAfterViewController", sender: self)
                        }
                        
                    }
                    }
                    break
                case 4: if(dfrh == true)
                {
                    if(tempDfrh == false)
                    {
                        self.choice = 4
                        loop = true
                        if(created == true)
                        {
                            performSegue(withIdentifier: "surveyAfterViewController", sender: self)
                        }else {
                            performSegue(withIdentifier: "instructionsAfterViewController", sender: self)
                        }
                        
                    }
                    }
                default:
                    break
                    }
                }
            
        }else{
            
            var loop = false
            while(loop == false)
            {
                var ran = arc4random_uniform(4) + 1
                switch(ran){
                case 1: if(sflh == true){
                    if(tempSflh == false)
                    {
                        self.choice = 1
                        loop = true
                        if(created == true)
                        {
                            performSegue(withIdentifier: "surveyAfterViewController", sender: self)
                        }else {
                            performSegue(withIdentifier: "instructionsAfterViewController", sender: self)
                        }
                        
                    }
                }
                    break
                case 2: if(sfrh == true)
                {
                    if(tempSfrh == false)
                    {
                        self.choice = 2
                        loop = true
                        if(created == true)
                        {
                            performSegue(withIdentifier: "surveyAfterViewController", sender: self)
                        }else {
                            performSegue(withIdentifier: "instructionsAfterViewController", sender: self)
                        }
                        
                    }
                }
                    break
                case 3: if(dflh == true)
                {
                    if(tempDflh == false)
                    {
                        self.choice = 3
                        loop = true
                        if(created == true)
                        {
                            performSegue(withIdentifier: "surveyAfterViewController", sender: self)
                        }else {
                            performSegue(withIdentifier: "instructionsAfterViewController", sender: self)
                        }
                        
                    }
                }
                    break
                case 4: if(dfrh == true)
                {
                    if(tempDfrh == false)
                    {
                        self.choice = 4
                        loop = true
                        if(created == true)
                        {
                            performSegue(withIdentifier: "surveyAfterViewController", sender: self)
                        }else {
                            performSegue(withIdentifier: "instructionsAfterViewController", sender: self)
                        }
                        
                    }
                    }
                default:
                    break
                }
            }
        }
        }
        }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "surveyAfterViewController")
        {
        var mainController = segue.destination as! SurveyViewController
        mainController.choice = self.choice
        mainController.visit = count
        mainController.task = tempTryCount + 1
        mainController.created = true
        }else {
            var mainController = segue.destination as! InstructionsViewController
            mainController.choice = self.choice
            mainController.visit = count
            mainController.task = tempTryCount + 1
            mainController.created = false
        }
        
    }
    
    @IBAction func contactTeamButtonPressed(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ContactResearchTeamNavigationViewController") as! ContactResearchTeamNavigationViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
    func uploadFile(completion: @escaping(Bool)->())
    {
        exportDatabase()
        completion(true)
    }
    func exportDatabase()
    {
        var tapString = createTapString()
        var visitString = createVisitString()
        var settingsString = createSettingsString()
        var notificationString = createNotificationString()
        saveAndExport(exportString: tapString,fileName: "Taps")
        saveAndExport(exportString: visitString, fileName: "Questionnaire")
        saveAndExport(exportString: settingsString, fileName: "Settings")
        saveAndExport(exportString: notificationString, fileName: "Notifications")
    }
    func createNotificationString() -> String
    {
        var exportString = "Notification Number,Date/Time\n"
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var notificationRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notification")
        notificationRequest.returnsObjectsAsFaults = false
        do{
            let notificationResults = try context.fetch(notificationRequest)
            if notificationResults.count > 0
            {
                for notificationResult in notificationResults as! [NSManagedObject]
                {
                    if let notificationNumber = notificationResult.value(forKey: "number") as? Int
                    {
                        exportString += String(notificationNumber) + ","
                        var tempDate = notificationResult.value(forKey: "date") as! Int
                        var date = Date(timeIntervalSince1970: TimeInterval(tempDate))
                        var formatter = DateFormatter()
                        formatter.dateFormat = "YYYY-MM-dd HH:MM:SS:MS"
                        var result = formatter.string(from: date)
                        exportString += result + "\n"
                    }
                }
            }
        } catch{
            
        }
        return exportString
    }
    func createSettingsString() -> String
    {
        var exportString: String = "Research Group ID,Subject ID,Task Range/Day,Alarm Start Time,Alarm Stop Time,Time Between Alarms(minutes),Single Finger Left Hand,Single Finger Right Hand,Double Finger Left hand,Double Finger Right Hand\n"
        let researchId = researchIdDefault.value(forKey: "researchid") as! String
        let subjectId = subjectIdDefault.value(forKey: "subjectid") as! String
        let taskRange = taskRangeDefault.value(forKey: "taskrange") as! String
        let alarmStart = startAlarmDefault.value(forKey: "startalarm") as! String
        let alarmStop = stopAlarmDefault.value(forKey: "stopalarm") as! String
        let timeBetween = timeBtwAlarmsDefault.value(forKey: "timebetween") as! String
        var sflh = "NO"
        var sfrh = "NO"
        var dflh = "NO"
        var dfrh = "NO"
        if(sflhDefault.value(forKey: "sflh") as! Bool == true)
        {
            sflh = "YES"
        }
        if(sfrhDefault.value(forKey: "sfrh") as! Bool == true)
        {
            sfrh = "YES"
        }
        if(dflhDefault.value(forKey: "dflh") as! Bool == true)
        {
            dflh = "YES"
        }
        if(dfrhDefault.value(forKey: "dfrh") as! Bool == true)
        {
            dfrh = "YES"
        }
        exportString += researchId + "," + subjectId + "," + taskRange + "," + alarmStart + "," + alarmStop + "," + timeBetween + "," + sflh + "," + sfrh + "," + dflh + "," + dfrh + "\n"
        return exportString
    }
    func createVisitString() -> String
    {
        var exportString: String = ""
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var visitRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Visit")
        visitRequest.returnsObjectsAsFaults = false
        do{
            let visitResults = try context.fetch(visitRequest)
            if visitResults.count > 0
            {
                exportString += "Visit Number,Date/Time,Abilty To Drive, Motor Activity, Motor State, Time Since Last Medicine(Hour.Minutes)\n"
                for visitResult in visitResults as! [NSManagedObject]
                {
                    if let visitNumber = visitResult.value(forKey: "number") as? Int
                    {
                        
                        let abilty = visitResult.value(forKey: "abilitytodrive")
                        let tempMotorActivity = visitResult.value(forKey: "motoractivity") as! Bool
                        let date = visitResult.value(forKey: "date") as! String
                        var motorActivity = ""
                        if(tempMotorActivity == true)
                        {
                            motorActivity = "YES"
                        }else{
                            motorActivity = "NO"
                        }
                        let tempMotorState = visitResult.value(forKey: "motorstate") as! Bool
                        var motorState = ""
                        if(tempMotorState == true)
                        {
                            motorState = "ON"
                        }
                        else{
                            motorState = "OFF"
                        }
                        var timeSinceLastMed = visitResult.value(forKey: "timesincelastmed") as! String

                        exportString += String(visitNumber) + "," + date + "," + String(describing: abilty!) + "," + motorActivity
                        exportString += "," + motorState + "," + String(describing: timeSinceLastMed) + "\n"
                    }
                }
            }
        }
        catch{
            
        }
        return exportString
    }
    func createTapString() -> String
    {
        
        var exportString: String = ""
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        exportString += "Tap Number,Distance(pt), Duration(ms),In Target,Tap Location X(pt),Tap Location Y(pt),Time, Time Lapsed(ms),"
        exportString += "Task Number,Visit Number,Finger,Hand,ScreenX(pt),ScreenY(pt),Time Zone,OrientationX(G),OrientationY(G),OrientationZ(G)\n"
        var visitRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Visit")
        visitRequest.returnsObjectsAsFaults = false
        var sortVisit = NSSortDescriptor.init(key: "number",ascending: true)
        visitRequest.sortDescriptors = [sortVisit]
        do{
            let visitResults = try context.fetch(visitRequest)
            if visitResults.count > 0
            {
                for visitResult in visitResults as! [NSManagedObject]
                {
                    if let visitNumber = visitResult.value(forKey: "number") as? Int
                    {
                        
                        var taskRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
                        taskRequest.returnsObjectsAsFaults = false
                        var sortTask = NSSortDescriptor.init(key: "number",ascending: true)
                        taskRequest.sortDescriptors = [sortTask]
                        do{
                            let taskResults = try context.fetch(taskRequest)
                            print("Task Results: ",taskResults.count)
                            if taskResults.count > 0
                            {
                                for taskResult in taskResults as! [NSManagedObject]
                                {
                                    if let tryNumber = taskResult.value(forKey: "number") as? Int
                                    {
                                        if let taskVisitNumber = taskResult.value(forKey: "visitnumber") as? Int
                                        {
                                            if(taskVisitNumber == visitNumber){
                                                
                                                let finger = taskResult.value(forKey: "finger")
                                                let hand = taskResult.value(forKey: "hand")
                                                let screenx = taskResult.value(forKey: "screenx")
                                                let screeny = taskResult.value(forKey: "screeny")
                        
                                                let timeZone = taskResult.value(forKey: "timezone")
                                                
                                                var tapRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Taps")
                                                tapRequest.returnsObjectsAsFaults = false
                                                var sortTap = NSSortDescriptor.init(key: "number",ascending: true)
                                                tapRequest.sortDescriptors = [sortTap]
                                                
                                                do{
                                                    let tapResults = try context.fetch(tapRequest)
                                                    if tapResults.count > 0
                                                    {
                                                        
                                                        for tapResult in tapResults as! [NSManagedObject]
                                                        {
                                                            if let tapNumber = tapResult.value(forKey: "number") as? Int
                                                            {
                                                                if let tapTaskNumber = tapResult.value(forKey: "tasknumber") as? Int
                                                                {
                                                                    let tapVisitNumber = tapResult.value(forKey: "visitnumber") as? Int
                                                                    if((tapTaskNumber == tryNumber) && (visitNumber == tapVisitNumber))
                                                                    {
                                                                        
                                                                        let distance = tapResult.value(forKey: "distance")
                                                                        let duration = tapResult.value(forKey: "duration")
                                                                        let tempInTarget = tapResult.value(forKey: "intarget") as! Bool
                                                                        var inTarget = ""
                                                                        if(tempInTarget == true)
                                                                        {
                                                                            inTarget = "YES"
                                                                        }else{
                                                                            inTarget = "NO"
                                                                        }
                                                                        let taplocationx = tapResult.value(forKey: "taplocationx")
                                                                        let taplocationy = tapResult.value(forKey: "taplocationy")
                                                                        let tapTime = tapResult.value(forKey: "time")
                                                                        let timeLapsed = tapResult.value(forKey: "timelapsed")
                                                                        let orientationX = tapResult.value(forKey: "orientationx")
                                                                        let orientationY = tapResult.value(forKey: "orientationy")
                                                                        let orientationZ = tapResult.value(forKey: "orientationz")
                                                                        exportString += String(tapNumber) + "," + String(describing: distance!) + ","
                                                                        exportString += String(describing: duration!) + "," + inTarget
                                                                        exportString += "," + String(describing: taplocationx!) + ","
                                                                        exportString += String(describing: taplocationy!) + ","
                                                                        
                                                                        exportString += String(describing: tapTime!) + ","
                                                                        exportString += String(describing: timeLapsed!) + ","
                                                                        exportString += String(tryNumber) + ","
                                                                        exportString += String(visitNumber) + ","
                                                                        exportString += String(describing: finger!) + ","
                                                                        exportString += String(describing: hand!) + ","
                                                                        exportString += String(describing: screenx!) + "," + String(describing: screeny!)
                                                                        exportString += "," + String(describing: timeZone!) + ","
                                                                        exportString += String(describing: orientationX!) + ","
                                                                        exportString += String(describing: orientationY!) + ","
                                                                        exportString += String(describing: orientationZ!) + "\n"
                                                                        
                                                                        
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                catch
                                                {
                                                    
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {
                            
                        }
                        
                        
                        
                    }
                }
            }
        }
        catch
        {
            
        }
        
        return exportString
    }
    func saveAndExport(exportString: String,fileName: String) {
        let exportFilePath = NSTemporaryDirectory() + "\(fileName).csv"
        let exportFileURL = URL(fileURLWithPath: exportFilePath)
        FileManager.default.createFile(atPath: exportFilePath, contents: NSData() as Data, attributes: nil)
        var fileHandleError: NSError? = nil
        var fileHandle: FileHandle? = nil
        do {
            fileHandle = try FileHandle(forWritingTo: exportFileURL as URL)
        } catch {
            print("Error with fileHandle")
        }
        
        if fileHandle != nil {
            fileHandle!.seekToEndOfFile()
            let csvData = exportString.data(using: String.Encoding.utf8, allowLossyConversion: false)
            fileHandle!.write(csvData!)
            
            fileHandle!.closeFile()
        }
        
        testUpload(export: exportFileURL,fileName: fileName)
        
        
    }
    func testUpload(export: URL,fileName: String) {
        
        let fileId = getFileID(fileName: fileName)
        print("File ID is: ",fileId)
        var endpoint = ""
        if(fileId.isEmpty)
        {
            endpoint = "https://upload.box.com/api/2.0/files/content"
        }
        else{
            endpoint = "https://upload.box.com/api/2.0/files/\(fileId)/content"
        }
        
        let elonPhotoURL = export
        print("Folder ID when uploading is: ",folderId)
        
        let finalDict = ["name": "\(fileName)", "parent": ["id": Int(folderId)]] as [String : Any]
        let fileUpload = Just.post(endpoint,data: ["name": "\(fileName)","parent.id": folderId], headers:["Authorization":"Bearer \(accessToken)","content-type":"application/json"], files:["\(fileName)": .url(elonPhotoURL, nil)])
        let json = fileUpload.json as! NSDictionary
    }

    func getFileID(fileName: String) -> String
    {
        var id = ""
        let fileSearch = Just.get("https://api.box.com/2.0/search?query=\(fileName)&file_extensions=csv&ancestor_folder_ids=\(folderId)", headers:["Authorization":"Bearer \(accessToken)"])
        let json = fileSearch.json as! NSDictionary
        let entries = json["entries"] as! Array<Dictionary<String, Any>>
        if(entries.count > 0)
        {
            id = entries[0]["id"] as! String
        }
        return id
        
    }
    
    
}

