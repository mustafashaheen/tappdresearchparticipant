//
//  SingleFingerViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 29/12/2017.
//  Copyright © 2017 UNMC. All rights reserved.
//

import UIKit
import CoreData
import CoreMotion
class SingleFingerViewController: UIViewController {


    @IBOutlet var instructionsLabel: UILabel!
    @IBOutlet var progressView: UIProgressView!
    var pressure = 0.0
    var tapCount = 0
    var countdown=19
    var progress = false
    var timer = Timer()
    @IBOutlet var tapCircleButton: UIButton!
    @IBOutlet var timerLabel: UILabel!
    var dfrhDefault = UserDefaults.standard
    var dflhDefault = UserDefaults.standard
    var sfrhDefault = UserDefaults.standard
    var sflhDefault = UserDefaults.standard
    var start = 0.0
    var end = 0.0
    var tempTime = 0.0
    var choice = 0
    var visit = 0
    var task = 0
    var inTarget = false
    var currentDate = Date()
    var motionManager = CMMotionManager()
    var sflh = false
    var sfrh = false
    var dflh = false
    var dfrh = false
    var tryCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapRecognizer)
        tapRecognizer.delegate = self as! UIGestureRecognizerDelegate
        self.navigationItem.hidesBackButton = true
        if(sflhDefault.value(forKey: "sflh") != nil){
            sflh = sflhDefault.value(forKey: "sflh") as! Bool
        }
        if(sfrhDefault.value(forKey: "sfrh") != nil){
            sfrh = sfrhDefault.value(forKey: "sfrh") as! Bool
        }
        if(dflhDefault.value(forKey: "dflh") != nil){
            dflh = dflhDefault.value(forKey: "dflh") as! Bool
        }
        if(dfrhDefault.value(forKey: "dfrh") != nil){
            dfrh = dfrhDefault.value(forKey: "dfrh") as! Bool
        }
        if(sflh == true)
        {
            tryCount += 1
        }
        if(sfrh == true)
        {
            tryCount += 1
        }
        if(dflh == true)
        {
            tryCount += 1
        }
        if(dfrh == true)
        {
            tryCount += 1
        }
        switch (choice) {
        case 1:
            instructionsLabel.text = "Use: Left Hand, Index Finger"
            break
        case 2:
            instructionsLabel.text = "Use: Right Hand, Index Finger"
        default:
            break
        }
        timerLabel.text = "Time Left: 19"
        if(countdown == 59){
            progress = false
        }
        else{
            progress = true
        }
        runTimer()
        addTryData()
        // Do any additional setup after loading the view.
    }
    @objc func tapped(gestureRecognizer: UITapGestureRecognizer) {
        var orientationX = 0.0
        var orientationY = 0.0
        var orientationZ = 0.0
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let newTap = NSEntityDescription.insertNewObject(forEntityName: "Taps", into: context)
        motionManager.startDeviceMotionUpdates(to: OperationQueue.current!) {(data,error) in
            if let myData = data {
                orientationX = myData.userAcceleration.x
                orientationY = myData.userAcceleration.y
                orientationZ = myData.userAcceleration.z
                var tempOrientation = String(format: "%.3f",orientationX)
                newTap.setValue(Double(tempOrientation), forKey: "orientationx")
                tempOrientation = String(format: "%.3f",orientationY)
                newTap.setValue(Double(tempOrientation), forKey: "orientationy")
                tempOrientation = String(format: "%.3f",orientationZ)
                newTap.setValue(Double(tempOrientation), forKey: "orientationz")
                self.motionManager.stopDeviceMotionUpdates()
            }
        }
        var newX = tapCircleButton.center.x
        var newY = tapCircleButton.center.y
        let touchPoint = gestureRecognizer.location(in: self.view)
        newX = newX - (touchPoint.x)
        newY = newY - (touchPoint.y)
        var floatX = Float(touchPoint.x)
        var floatY = Float(touchPoint.y)
        let newYString = String(format: "%.2f", floatY)
        let newXString = String(format: "%.2f", floatX)
        
        floatX = Float(newXString)!
        floatY = Float(newYString)!
        var tempDistance = sqrt((pow(newX, 2)) + (pow(newY, 2)))
        var distance = String(format: "%.2f",tempDistance)
        tapCount += 1
        if(gestureRecognizer.state == .began)
        {
            start = Double(NSDate().timeIntervalSince1970)
        }else if(gestureRecognizer.state == .ended)
        {
            end = Double(NSDate().timeIntervalSince1970)
        }
        var tempDuration = end - start
        var duration = String(format: "%.2f",tempDuration)
        inTarget = false
        var timeLapsed = 0.0
        
        if(tapCount != 1){
            timeLapsed = end - tempTime
            tempTime = start
        }else{
            tempTime = start
        }
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd HH:MM:SS:MS"
        let result = formatter.string(from: date)
        var time = result
        let tempLapsed = String(format: "%.4f", timeLapsed)
        timeLapsed = Double(tempLapsed)!
        newTap.setValue(tapCount, forKey: "number")
        newTap.setValue(Double(duration), forKey: "duration")
        newTap.setValue(pressure, forKey: "pressure")
        newTap.setValue(timeLapsed, forKey: "timelapsed")
        newTap.setValue(floatX, forKey: "taplocationx")
        newTap.setValue(floatY, forKey: "taplocationy")
        newTap.setValue(time, forKey: "time")
        newTap.setValue(task, forKey: "tasknumber")
        newTap.setValue(inTarget, forKey: "intarget")
        newTap.setValue(Double(distance), forKey: "distance")
        newTap.setValue(visit, forKey: "visitnumber")
        print("Tap Number: ",tapCount)
        do{
            try context.save()
            
        }catch{
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buyTap(_ sender: Any, forEvent event: UIEvent) {
        var orientationX = 0.0
        var orientationY = 0.0
        var orientationZ = 0.0
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let newTap = NSEntityDescription.insertNewObject(forEntityName: "Taps", into: context)
        motionManager.startDeviceMotionUpdates(to: OperationQueue.current!) {(data,error) in
            if let myData = data {
                
                orientationX = myData.userAcceleration.x
                orientationY = myData.userAcceleration.y
                orientationZ = myData.userAcceleration.z
               var tempOrientation = String(format: "%.3f",orientationX)
                newTap.setValue(Double(tempOrientation), forKey: "orientationx")
                tempOrientation = String(format: "%.3f",orientationY)
                newTap.setValue(Double(tempOrientation), forKey: "orientationy")
                tempOrientation = String(format: "%.3f",orientationZ)
                newTap.setValue(Double(tempOrientation), forKey: "orientationz")
                
                self.motionManager.stopDeviceMotionUpdates()
            }
        }
        end = Double(NSDate().timeIntervalSince1970)
        tapCircleButton.backgroundColor = UIColor.white
        tapCount += 1
        inTarget = true
        let myButton = sender as! UIButton
        var newX = myButton.center.x
        var newY = myButton.center.y
        let touches = event.touches(for: myButton)
        let touch = touches?.first
        var touchPoint = touch?.location(in: myButton)
        newX = newX - (touchPoint?.x)!
        newY = newY - (touchPoint?.y)!
        var tempDistance = sqrt((pow(newX, 2)) + (pow(newY, 2)))
        var distance = String(format: "%.2f",tempDistance)
        var floatX = Float((touchPoint?.x)!)
        var floatY = Float((touchPoint?.y)!)
        let newYString = String(format: "%.2f", floatY)
        let newXString = String(format: "%.2f", floatX)
        
        floatX = Float(newXString)!
        floatY = Float(newYString)!
        var tempDuration = end - start
        var duration = String(format: "%.2f",tempDuration)
        var number = tapCount
        var timeLapsed = 0.0
        
        if(tapCount != 1){
            timeLapsed = end - tempTime
            tempTime = start
        }else{
            tempTime = start
        }
        let tempLapsed = String(format: "%.4f", timeLapsed)
        timeLapsed = Double(tempLapsed)!
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd HH:MM:SS:MS"
        let result = formatter.string(from: date)
        var time = result
        newTap.setValue(number, forKey: "number")
        newTap.setValue(Double(distance), forKey: "distance")
        newTap.setValue(Double(duration), forKey: "duration")
        newTap.setValue(pressure, forKey: "pressure")
        newTap.setValue(timeLapsed, forKey: "timelapsed")
        newTap.setValue(floatX, forKey: "taplocationx")
        newTap.setValue(floatY, forKey: "taplocationy")
        newTap.setValue(time, forKey: "time")
        newTap.setValue(inTarget, forKey: "intarget")
        newTap.setValue(task, forKey: "tasknumber")
        newTap.setValue(visit, forKey: "visitnumber")
         print("Tap Number: ",number)
        do{
            try context.save()
            
        }catch{
            
        }
        
    }
    func addTryData(){
        var bounds = UIScreen.main.bounds
        var width = bounds.size.width
        var height = bounds.size.height
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd HH:MM:SS"
        var time = formatter.string(from: currentDate)
        var timeZone = TimeZone.current.identifier
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let newTask = NSEntityDescription.insertNewObject(forEntityName: "Task", into: context)
        newTask.setValue(task, forKey: "number")
        newTask.setValue(time, forKey: "time")
        newTask.setValue(timeZone, forKey: "timezone")
        newTask.setValue(visit, forKey: "visitnumber")
        newTask.setValue(Float(width), forKey: "screenx")
        newTask.setValue(Float(height), forKey: "screeny")
        switch(choice) {
        case 1:
            newTask.setValue("single", forKey: "finger")
            newTask.setValue("left", forKey: "hand")
            break
        case 2:
            newTask.setValue("single", forKey: "finger")
            newTask.setValue("right", forKey: "hand")
            break
        case 3:
            newTask.setValue("double", forKey: "finger")
            newTask.setValue("left", forKey: "hand")
            break
        case 4:
            newTask.setValue("double", forKey: "finger")
            newTask.setValue("right", forKey: "hand")
            break
        default:
            break
        }
        do{
            try context.save()
            
        }catch{
            
        }

    }
    func showViewController(action: UIAlertAction){
        var tempSflh = false
        var tempSfrh = false
        var tempDflh = false
        var tempDfrh = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        request.returnsObjectsAsFaults = false
        do{
            let results = try context.fetch(request)
            var count = 0
            for result in results as! [NSManagedObject] {
                let visitNumber = result.value(forKey: "visitnumber") as? Int
                if(visit == visitNumber)
                {
                    count += 1
                    if let finger = result.value(forKey: "finger") as? String
                    {
                        if let hand = result.value(forKey: "hand") as? String
                        {
                            if(finger == "single") && (hand == "left")
                            {
                                tempSflh = true
                            }
                            if(finger == "single") && (hand == "right")
                            {
                                tempSfrh = true
                            }
                            if(finger == "double") && (hand == "left")
                            {
                                tempDflh = true
                            }
                            if(finger == "double") && (hand == "right")
                            {
                                tempDfrh = true
                            }
                        }
                    }
                }
                
            }
            if(count == tryCount)
            {
                performSegue(withIdentifier: "viewControllerAfterSingleFinger", sender: self)
            }else{
                var loop = false
                while(loop == false)
                {
                    var ran = arc4random_uniform(4) + 1
                    
                    switch(ran)
                    {
                    case 1:
                        if((sflh == true) && (tempSflh == false))
                        {
                            loop = true
                            self.choice = 1
                            performSegue(withIdentifier: "specialInstructionsAfterSingleFinger", sender: self)
                        }
                        break
                    case 2:
                        if((sfrh == true) && (tempSfrh == false))
                        {
                            loop = true
                            self.choice = 2
                            performSegue(withIdentifier: "specialInstructionsAfterSingleFinger", sender: self)
                        }
                        break
                    case 3:
                        if((dflh == true) && (tempDflh == false))
                        {
                            loop = true
                            self.choice = 3
                            performSegue(withIdentifier: "specialInstructionsAfterSingleFinger", sender: self)
                        }
                        break
                    case 4:
                        if((dfrh == true) && (tempDfrh == false))
                        {
                            loop = true
                            self.choice = 4
                            performSegue(withIdentifier: "specialInstructionsAfterSingleFinger", sender: self)
                        }
                        break
                    default:
                        break
                    }
                }
            }
        }
        catch
        {
            
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "specialInstructionsAfterSingleFinger")
        {
            var mainController = segue.destination as! SpecialInstructionsViewController
            mainController.choice = choice
            mainController.visit = visit
            mainController.task = task + 1
        }else{
        var mainController = segue.destination as! ViewController
        mainController.send = true
        }
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(SingleFingerViewController.updateTimer)), userInfo: nil, repeats: true)
        
    }
    @objc func updateTimer() {
        
        if(progress == true)
        {
            progressView.progress += 0.053
        }
        else{
            progressView.progress += 0.0171
        }
        if(countdown < 1){
            timer.invalidate()
            tapCircleButton.isEnabled = false
            let alert = UIAlertController(title: "Test Completed", message: "Well done, you performed \n\n \(tapCount) taps", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: showViewController))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            countdown -= 1     //This will decrement(count down)the seconds.
            if(countdown<10)
            {
                timerLabel.text = "Time Left: 0\(countdown)"
            }
            else{
                timerLabel.text = "Time Left: \(countdown)" //This will update the label.
            }
        }
        
    }
    
    
    @IBAction func touchDownOfButton(_ sender: RoundedButton) {
        start = Double(NSDate().timeIntervalSince1970)
        
        tapCircleButton.backgroundColor = UIColor(red: 74/255, green: 237/255, blue: 38/255, alpha: 1.00)
        //var touch = ForceTouchGestureRecognizer()
        //pressure = Float(touch.force/touch.maximumForce)
        
    }

}
extension SingleFingerViewController : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.superview!.superclass! .isSubclass(of: UIButton.self) {
            return false
        }
        return true
}
}

