//
//  SpecialInstructionsViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 13/01/2018.
//  Copyright © 2018 UNMC. All rights reserved.
//

import UIKit

class SpecialInstructionsViewController: UIViewController {

    @IBOutlet var secondLabel: UILabel!
    @IBOutlet var firstLabel: UILabel!
    var choice = 0
    var task = 0
    var visit = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        switch (choice) {
        case 1:
            firstLabel.text = "In this task, you will use your left hand."
            secondLabel.text = "Tap as fast as you can using just your index finger."
            break
        case 2:
            firstLabel.text = "In this task, you will use your right hand."
            secondLabel.text = "Tap as fast as you can using just your index finger."
            break
        case 3:
            firstLabel.text = "In this task, you will use your left hand."
            secondLabel.text = "Tap as fast as you can by alternating between your index and middle finger."
            break
        case 4:
            firstLabel.text = "In this task, you will use your right hand."
            secondLabel.text = "Tap as fast as you can by alternating between your index and middle finger."
            break
        default:
            break
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startButtonPressed(_ sender: RoundedButton) {
        performSegue(withIdentifier: "timerAfterSpecialInstructions", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

            var mainController = segue.destination as! TimerViewController
            mainController.choice = choice
            mainController.visit = visit
            mainController.task = task
        
        
    }
    

}
