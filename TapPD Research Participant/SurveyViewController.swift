//
//  SurveyViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 20/12/2017.
//  Copyright © 2017 UNMC. All rights reserved.
//

import UIKit
import CoreData
class SurveyViewController:UIViewController{
    @IBOutlet weak var abilityToDrive: UISegmentedControl!
    @IBOutlet weak var motorActivityNoButton: UIButton!
    @IBOutlet weak var motorActivityYesButton: UIButton!
    @IBOutlet weak var motorStateOffButton: UIButton!
    @IBOutlet weak var motorStateOnButton: UIButton!
    
    @IBOutlet weak var timeSinceLastMed: UITextField!
    var back = false
    var choice = 0
    var visit = 0
    var task = 0
    var motorActivity = false
    var motorState = false
    var ability = 0
    var created = false
    let datePicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        createDatePicker()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        var request = NSFetchRequest<NSFetchRequestResult>(entityName: "Visit")
        request.returnsObjectsAsFaults = false
        do{
            let results = try context.fetch(request)
            if results.count > 0
            {
                for result in results as! [NSManagedObject]
                {
                    if let visitNumber = result.value(forKey: "number") as? Int
                    {
                        if(visitNumber == visit)
                        {
                            back = true
                            if(result.value(forKey: "timesincelastmed") as? String != nil)
                            {
                                timeSinceLastMed.text = result.value(forKey: "timesincelastmed") as? String
                            }
                            if(result.value(forKey: "motoractivity") as? Bool != nil)
                            {
                                if(result.value(forKey: "motoractivity") as? Bool == true)
                                {
                                motorActivityYesButton.setImage(UIImage(named: "radiobutton-checked"), for: .normal)
                                }else{
                                motorActivityNoButton.setImage(UIImage(named: "radiobutton-checked"), for: .normal)
                                }
                            }
                            if(result.value(forKey: "motorstate") as? Bool != nil)
                            {
                                if(result.value(forKey: "motorstate") as? Bool == true)
                                {
                                    motorStateOnButton.setImage(UIImage(named: "radiobutton-checked"), for: .normal)
                                }else{
                                    motorStateOffButton.setImage(UIImage(named: "radiobutton-checked"), for: .normal)
                                }
                            }
                            if(result.value(forKey: "abilitytodrive") as? Int != nil)
                            {
                                abilityToDrive.selectedSegmentIndex = (result.value(forKey: "abilitytodrive") as? Int)! - 1
                            }
                            
                        }
                        
                    }
                }
            }
        }
        catch
        {
            
        }

    }
    @IBAction func finishButtonPressed(_ sender: RoundedButton) {
        var count = 0
        if(timeSinceLastMed.text?.isEmpty)!
        {
            count += 1
            displayAlarm(message: "Please input time since your last medicine")
        } else
        if((motorStateOnButton.currentImage?.isEqual(UIImage(named: "radiobutton-unchecked")))! && (motorStateOffButton.currentImage?.isEqual(UIImage(named: "radiobutton-unchecked")))!)
        {
            count += 1
            displayAlarm(message: "Please select your current motor state")
        } else
        if((motorActivityYesButton.currentImage?.isEqual(UIImage(named: "radiobutton-unchecked")))! && (motorActivityNoButton.currentImage?.isEqual(UIImage(named: "radiobutton-unchecked")))!)
        {
            count += 1
            displayAlarm(message: "Please select you're having excessive motor activity")
        } else {
      
            if(count == 0)
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let context = appDelegate.persistentContainer.viewContext
                ability = Int(abilityToDrive.titleForSegment(at: abilityToDrive.selectedSegmentIndex)!)!
                print(ability)
                if(back == false)
                {
            let currentDate = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-MM-dd HH:MM:SS:MS"
            let result = formatter.string(from: currentDate)
            
            let newVisit = NSEntityDescription.insertNewObject(forEntityName: "Visit", into: context)
            newVisit.setValue(visit, forKey: "number")
            newVisit.setValue(result, forKey: "date")
            newVisit.setValue(timeSinceLastMed.text, forKey: "timesincelastmed")
            newVisit.setValue(motorState, forKey: "motorstate")
            newVisit.setValue(motorActivity, forKey: "motoractivity")
            newVisit.setValue(ability, forKey: "abilitytodrive")
        
            }
                else{
                    var request = NSFetchRequest<NSFetchRequestResult>(entityName: "Visit")
                    request.returnsObjectsAsFaults = false
                    do{
                        let results = try context.fetch(request)
                        if results.count > 0
                        {
                            for result in results as! [NSManagedObject]
                            {
                                if let visitNumber = result.value(forKey: "number") as? Int
                                {
                                    if(visitNumber == visit)
                                    {
                                        result.setValue(ability, forKey: "abilitytodrive")
                                    result.setValue(timeSinceLastMed.text, forKey: "timesincelastmed")
                                        result.setValue(motorState, forKey: "motorstate")
                                        result.setValue(motorActivity, forKey: "motoractivity")
                                    }
                                    
                                }
                            }
                        }
                    }
                    catch
                    {
                        
                    }
                }
                do{
                    try context.save()
                    
                }catch{
                    
                }
                performSegue(withIdentifier: "instructionsAfterSurveyController", sender: self)
            }
        }
    }
    func displayAlarm(message: String){
    let alert = UIAlertController(title: "Failure!", message: message, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil))
    self.present(alert, animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var mainController = segue.destination as! InstructionsViewController
        mainController.choice = choice
        mainController.visit = visit
        mainController.task = task
        mainController.created = created
        
    }
    
    @IBAction func motorStateOffButtonPressed(_ sender: UIButton) {
        if(sender.currentImage?.isEqual(UIImage(named: "radiobutton-unchecked")))!{
            sender.setImage(UIImage(named: "radiobutton-checked"), for: .normal)
            motorStateOnButton.setImage(UIImage(named: "radiobutton-unchecked"), for: .normal)
            motorState = false
        }
        
    }
    @IBAction func motorStateOnButtonPressed(_ sender: UIButton) {
        if(sender.currentImage?.isEqual(UIImage(named: "radiobutton-unchecked")))!{
            sender.setImage(UIImage(named: "radiobutton-checked"), for: .normal)
            motorStateOffButton.setImage(UIImage(named: "radiobutton-unchecked"), for: .normal)
            motorState = true
        }
    }
    
    @IBAction func motorActivityYesButtonPressed(_ sender: UIButton) {
        if(sender.currentImage?.isEqual(UIImage(named: "radiobutton-unchecked")))!{
            sender.setImage(UIImage(named: "radiobutton-checked"), for: .normal)
            motorActivityNoButton.setImage(UIImage(named: "radiobutton-unchecked"), for: .normal)
            motorActivity = true
        }
    }
    
    @IBAction func lastMedEditStart(_ sender: UITextField) {
         timeSinceLastMed.inputView = datePicker
        
    }
    @objc func donePressed(){
        let date = datePicker.date
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let result = formatter.string(from: date)
        timeSinceLastMed.text = result
        self.view.endEditing(true)
    }
    func createDatePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done,target: nil,action: #selector(donePressed))
        toolbar.setItems([doneButton],animated: false)
        timeSinceLastMed.inputAccessoryView = toolbar

        datePicker.datePickerMode = UIDatePickerMode.time
    }
    @IBAction func motorActivityNoButtonPressed(_ sender: UIButton) {
        if(sender.currentImage?.isEqual(UIImage(named: "radiobutton-unchecked")))!{
            sender.setImage(UIImage(named: "radiobutton-checked"), for: .normal)
            motorActivityYesButton.setImage(UIImage(named: "radiobutton-unchecked"), for: .normal)
            motorActivity = false
        }
    }
}
