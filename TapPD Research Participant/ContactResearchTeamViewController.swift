//
//  ContactResearchTeamViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 19/12/2017.
//  Copyright © 2017 UNMC. All rights reserved.
//

import UIKit
import Foundation
import MessageUI
class ContactResearchTeamViewController: UIViewController,MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func emailButtonPressed(_ sender: RoundedButton) {
        if( MFMailComposeViewController.canSendMail() ) {
            print("Can send email.")
            
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            

            mailComposer.setToRecipients(["mbhl@unmc.edu"])
            
            
            
            self.present(mailComposer, animated: true, completion: nil)
        }
    }
    
    @IBAction func callButtonPresed(_ sender: RoundedButton) {
        
        if let url = NSURL(string: "tel://402-559-6870"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}
