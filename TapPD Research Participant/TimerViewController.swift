//
//  TimerViewController.swift
//  TapPD Research Participant
//
//  Created by Mustafa Shaheen on 29/12/2017.
//  Copyright © 2017 UNMC. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {

    @IBOutlet var startButton: UIButton!
    var choice = 0
    var task = 0
    var visit = 0
    var countdown=3
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        print("choice in timer is: \(choice)")
        startButton.isEnabled = false
        startButton.setTitleColor(UIColor.init(red: 19/255, green: 118/255, blue: 251/255, alpha: 1.0), for: .normal)
        runTimer()
        // Do any additional setup after loading the view.
    }
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(TimerViewController.updateTimer)), userInfo: nil, repeats: true)
        
    }
    @objc func updateTimer() {
        if(countdown < 1){
            timer.invalidate()
            if(choice < 3){
                performSegue(withIdentifier: "SingleFingerTime", sender: self)
            }
            else{
                performSegue(withIdentifier: "DoubleFingerTime", sender: self)
            }
        }
        else{
            countdown -= 1     //This will decrement(count down)the seconds.
            startButton.setTitle("\(countdown)", for: .normal) //This will update the label.
        }
        
    }
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(choice < 3) {
            var mainController = segue.destination as! SingleFingerViewController
            mainController.choice = choice
            mainController.visit = visit
            mainController.task = task
        }
        else{
            var mainController = segue.destination as! DoubleFingerViewController
            mainController.choice = choice
            mainController.visit = visit
            mainController.task = task
        }
        
    }

}
